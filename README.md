# GigaPixelImaging

This repository contains the code associated with: "Gigapixel behavioral and neural activity imaging with a novel multi-camera array microscope."
Eric E Thomson1, Mark Harfouche3, Pavan C Konda2, Catherine Seitz1, Kanghyun Kim2, Colin Cooke2, Shiqi Xu2, Whitney S Jacobs1, Robin Blazing1, Yang Chen1, Sunanda Sharma3, Timothy W Dunn2, Jaehee Park3, Roarke Horstmeyer2, 3* & Eva A Naumann1*
Affiliations:
1 Duke School of Medicine Department of Neurobiology, Durham, NC 27710
2 Biomedical Engineering, Duke University, Durham, NC 27710
3 Ramona Optics Inc., Durham, NC 27701

*Corresponding Authors/These authors jointly supervised this work:
eva.naumann@duke.edu
roarke.w.horstmeyer@duke.edu

Submittd for review to eLife 1/2022

Detailed instuctions to run each analysis are contained in the docstrings of file headers or in README files within each folder. 

Contents
- stitching_via_template
- video_creation_from_stitched_frames 
- 3d_depth_estimation
- fish_identification_code
- eye_position_tracking
- single_fluorescence_zebrafish
- dual_fluorescence_zebrafish 
- c_elegans_segmentation_code
- c_elegans_density_estimation 

- Object_detection_code: Please see different Github repository at https://github.com/EricThomson/gigadetector

