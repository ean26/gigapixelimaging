
clear; clc;

FolB = 'X:\Data_backup\01_GigaCam_NatMet_Paper\06_Adult_Flies\01_Stitched_Data\';

Fol = [FolB 'stitched\'];

%%
I4kx = 2160;
I4ky = 3840;

tic

v = VideoWriter([FolB 'Video_try1\Video_5fps.avi']);
v.FrameRate = 5;
open(v);

%% n=0;
for n=0:249

% Load the image
n_pad = sprintf( '%04d', n);
disp(n_pad);
FileN = ['movie0_stitched_' n_pad '.tif'];
I = 2.8*imread([Fol FileN]);

%% Cropping parameters
Img4k = uint8(255*ones(I4kx,I4ky,3));

I1_sf = 11;
I2_sf = 4;
I3_sf = 1;

I2_cropD = 1000;
I3_cropD = 1000; % Resized crop size to fit on the full frame
I3_cropA = 500; % Actual crop size

Mag = 0.155;
PixS = 9e-3; % Effective pixel size in mm, i.e., Act_Pix/Mag

I2_c1x = 11300;
I2_c1y = 8000;
I2_c2x = 17500;
I2_c2y = 11300;

I3_c1x = I2_c1x+1750;
I3_c1y = I2_c1y+3200;
I3_c2x = I2_c2x+2200;
I3_c2y = I2_c2y+1350;

%% Full fov resizing and annotation

I1_scale = 30; % in mm
I1_scale_h = 20; % in pixels
I1_scale_xps = 370; % x position shift from bottom right corner
I1_scale_yps = 60; % y position shift from bottom right corner
I1_txt_spc = 100;
I1_txt_xps = -15;

% Downsample the image and adjust contrast
Img1 = I(1:I1_sf:end,1:I1_sf:end,1:3);
[I1x, I1y, I1z] = size(Img1);
% Add the annotation boxes to the image
position1 = [round(I2_c1y/I1_sf) round(I2_c1x/I1_sf) round(I2_cropD*(I2_sf/I1_sf)) round(I2_cropD*(I2_sf/I1_sf)); round(I2_c2y/I1_sf) round(I2_c2x/I1_sf) round(I2_cropD*(I2_sf/I1_sf)) round(I2_cropD*(I2_sf/I1_sf))];
Img1Ann = insertShape((Img1),'Rectangle',position1,'LineWidth',12,'Color', {'red','green'});
position2 = [round(I3_c1y/I1_sf) round(I3_c1x/I1_sf) round(I3_cropA*(I3_sf/I1_sf)) round(I3_cropA*(I3_sf/I1_sf)); round(I3_c2y/I1_sf) round(I3_c2x/I1_sf) round(I3_cropA*(I3_sf/I1_sf)) round(I3_cropA*(I3_sf/I1_sf))];
Img1Ann = insertShape((Img1Ann),'Rectangle',position2,'Color', {'cyan','magenta'},'LineWidth',10);

% Add the scale bar to the image
Img1Ann = insertShape(Img1Ann,'FilledRectangle',[I1y-I1_scale_xps I1x-I1_scale_yps round(I1_scale/(PixS*I1_sf)) I1_scale_h],'Color', {'red'},'Opacity',1);
Img1Ann = insertText(Img1Ann,[I1y-I1_scale_xps-I1_txt_xps I1x-I1_scale_yps-I1_txt_spc],[num2str(I1_scale) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');
% figure(7); subplot(2,5,[1,2,6,7]); imshow(Img1Ann); axis image; axis off;

% Update the 4k full image
I1_Trim = 0;
Img4k(:,1+50:I1y-I1_Trim,:) = Img1Ann(floor((I1x-I4kx)/2):floor((I1x+I4kx)/2)-1,1+50:I1y-I1_Trim,:); 
% figure(1); imshow(Img4k); axis image; 

%% First crops with annotations

Img2 = I(I2_c1x:I2_sf:I2_c1x+I2_sf*I2_cropD-1, I2_c1y:I2_sf:I2_c1y+I2_sf*I2_cropD-1,1:3);
Img2 = insertShape(Img2,'Rectangle',[1 1 I2_cropD I2_cropD],'LineWidth',15,'Color', {'red'});

% Add the annotation boxes to the image
position1 = [round((I3_c1y-I2_c1y)/I2_sf) round((I3_c1x-I2_c1x)/I2_sf) round(I3_cropA*(I3_sf/I2_sf)) round(I3_cropA*(I3_sf/I2_sf))];
Img2 = insertShape(Img2,'Rectangle',position1,'LineWidth',6,'Color', {'cyan'});

Img4k(41:40+I2_cropD,1751:1750+I2_cropD,:) = Img2; %figure(1); imshow(Img4k); axis image; 

%
Img3 = I(I2_c2x:I2_sf:I2_c2x+I2_sf*I2_cropD-1, I2_c2y:I2_sf:I2_c2y+I2_sf*I2_cropD-1,1:3);
Img3 = insertShape(Img3,'Rectangle',[1 1 I2_cropD I2_cropD],'LineWidth',15,'Color', {'green'});

% Add the annotation boxes to the image
position1 = [round((I3_c2y-I2_c2y)/I2_sf) round((I3_c2x-I2_c2x)/I2_sf) round(I3_cropA*(I3_sf/I2_sf)) round(I3_cropA*(I3_sf/I2_sf))];
Img3 = insertShape(Img3,'Rectangle',position1,'LineWidth',6,'Color', {'magenta'});

% Add the scale bar to the image
I2_scale = 10; % in mm
I2_scale_h = 20; % in pixels
I2_scale_xps = 350; % x position shift from bottom right corner
I2_scale_yps = 50; % y position shift from bottom right corner
I2_txt_spc = 100;
I2_txt_xps = 0;

Img3 = insertShape(Img3,'FilledRectangle',[I2_cropD-I2_scale_xps I2_cropD-I2_scale_yps round(I2_scale/(PixS*I2_sf)) I2_scale_h],'Color', {'red'},'Opacity',1);
Img3 = insertText(Img3,[I2_cropD-I2_scale_xps-I2_txt_xps I2_cropD-I2_scale_yps-I2_txt_spc],[num2str(I2_scale) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');

Img4k(I2_cropD+121:I2_cropD+120+I2_cropD,1751:1750+I2_cropD,:) = Img3; %figure(1); imshow(Img4k); axis image; 

%% second crops

Img4 = I(I3_c1x:I3_sf:I3_c1x+I3_sf*I3_cropA-1, I3_c1y:I3_sf:I3_c1y+I3_sf*I3_cropA-1,1:3);
Img4 = imresize(Img4,I3_cropD/I3_cropA);
Img4 = insertShape(Img4,'Rectangle',[1 1 I3_cropD I3_cropD],'LineWidth',15,'Color', {'cyan'});

Img4k(41:40+I3_cropD,1751+I2_cropD+50:1750+I2_cropD+50+I3_cropD,:) = Img4; %figure(1); imshow(Img4k); axis image; 

%
Img5 = I(I3_c2x:I3_sf:I3_c2x+I3_sf*I3_cropA-1, I3_c2y:I3_sf:I3_c2y+I3_sf*I3_cropA-1,1:3);
Img5 = imresize(Img5,I3_cropD/I3_cropA);
Img5 = insertShape(Img5,'Rectangle',[1 1 I3_cropD I3_cropD],'LineWidth',15,'Color', {'magenta'});

% Add the scale bar to the image
I3_scale = 1; % in mm
I3_scale_h = 20; % in pixels
I3_scale_xps = 300; % x position shift from bottom right corner
I3_scale_yps = 50; % y position shift from bottom right corner
I3_txt_spc = 100;
I3_txt_xps = 10;

Img5 = insertShape(Img5,'FilledRectangle',[I3_cropD-I3_scale_xps I3_cropD-I3_scale_yps round(I3_scale/(PixS/(I3_cropD/I3_cropA)*I3_sf)) I3_scale_h],'Color', {'red'},'Opacity',1);
Img5 = insertText(Img5,[I3_cropD-I3_scale_xps-I3_txt_xps I3_cropD-I3_scale_yps-I3_txt_spc],[num2str(I3_scale) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');

Img4k(I3_cropD+121:I3_cropD+120+I3_cropD,1751+I2_cropD+50:1750+I2_cropD+50+I3_cropD,:) = Img5;
figure(1); imshow(Img4k); axis image;
%%
% Img4k = insertText(Img4k,[760 10],['Frame number: ' n_pad],'BoxColor','white','BoxOpacity',1,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','black');  figure(1); imshow(Img4k); axis image; 

FileN2 = [FolB 'Video_try1\movie0_stitched_' n_pad '_videoSnap.png'];
imwrite(Img4k,FileN2);

Img4k = insertText(Img4k,[1000 10],['Frame number: ' n_pad],'BoxColor','white','BoxOpacity',0.8,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','black');  
% figure(1); imshow(Img4k); axis image; 
writeVideo(v, Img4k);
%%
end
close(v);

figure(1); imshow(Img4k); axis image;


toc