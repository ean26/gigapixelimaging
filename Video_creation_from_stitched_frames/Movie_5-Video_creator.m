
clear; clc;

FolB = 'Z:\Data_backup\01_GigaCam_NatMet_Paper\08_Fluorescence_Fish\';

Fol_stitched = [FolB '90_Other_unorganized\' 'all_fishes_segmentation_gr_sorted\'];
Fol_fish0_green = [FolB '90_Other_unorganized\' 'all_fishes_rotated\' 'fish0_rotated\' 'green\'];
Fol_fish0_red = [FolB '90_Other_unorganized\' 'all_fishes_rotated\' 'fish0_rotated\' 'red\'];
Fol_fish1_green = [FolB '90_Other_unorganized\' 'all_fishes_rotated\' 'fish1_rotated\' 'green\'];
Fol_fish1_red = [FolB '90_Other_unorganized\' 'all_fishes_rotated\' 'fish1_rotated\' 'red\'];
Fol_fish2_green = [FolB '90_Other_unorganized\' 'all_fishes_rotated\' 'fish2_rotated\' 'green\'];
Fol_fish2_red = [FolB '90_Other_unorganized\' 'all_fishes_rotated\' 'fish2_rotated\' 'red\'];


%%
I4kx = 2160;
I4ky = 3840;
rotate_idx = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,75,95,122,126,135,153,158,166,177,191,202,217,218,219,244];

tic

v = VideoWriter([FolB '01_Stitched_Data\' 'Video_try2\Video_5fps.avi']);
v.FrameRate = 5;
open(v);

%% n=0;
I_stitched = [];
I_fish0_green = [];
I_fish0_red = [];
I_fish1_green = [];
I_fish1_red = [];
I_fish2_green = [];
I_fish2_red = [];

for n=0:250

% Load the image
% n_pad = sprintf( '%04d', n);
n_pad = n;
disp(n_pad);
FileN = ['frame_' num2str(n_pad) '.jpg'];
I_stitched = try_and_except(I_stitched, [Fol_stitched FileN]);
I_fish0_green = try_and_except(I_fish0_green, [Fol_fish0_green FileN]);
I_fish0_red = try_and_except(I_fish0_red, [Fol_fish0_red FileN]);
I_fish1_green = try_and_except(I_fish1_green, [Fol_fish1_green FileN]);
I_fish1_red = try_and_except(I_fish1_red, [Fol_fish1_red FileN]);
I_fish2_green = try_and_except(I_fish2_green, [Fol_fish2_green FileN]);
I_fish2_red = try_and_except(I_fish2_red, [Fol_fish2_red FileN]);

if ismember(n, rotate_idx)
    I_fish0_red = imrotate(I_fish0_red, 180);
end

%% Cropping parameters
Img4k = uint8(255*ones(I4kx,I4ky,3));

I1_sf = 5;
I2_sf = 1;

I2_cropD = 401;
% I3_cropD = 1000; % Resized crop size to fit on the full frame
% I3_cropA = 500; % Actual crop size

% Mag = 0.155;
PixS = 9e-3; % Effective pixel size in mm, i.e., Act_Pix/Mag

% I2_c1x = 5300;
% I2_c1y = 13700;
% I2_c2x = 17500;
% I2_c2y = 1300;
% 
% I3_c1x = I2_c1x+1700;
% I3_c1y = I2_c1y+500;
% I3_c2x = I2_c2x+2550;
% I3_c2y = I2_c2y+450;

%% Full fov resizing and annotation

I1_scale = 10; % in mm
I1_scale_h = 1200; % in pixels
I1_scale_xps = 270; % x position shift from bottom right corner
I1_scale_yps = 100; % y position shift from bottom right corner
I1_txt_spc = 100;
I1_txt_xps = 15;

% Downsample the image and adjust contrast
Img1 = I_stitched(1:I1_sf:end,1:I1_sf:end,1:3);
[I1x, I1y, I1z] = size(Img1);
% Add the annotation boxes to the image
% position1 = [round(I2_c1y/I1_sf) round(I2_c1x/I1_sf) round(I2_cropD*(I2_sf/I1_sf)) round(I2_cropD*(I2_sf/I1_sf)); round(I2_c2y/I1_sf) round(I2_c2x/I1_sf) round(I2_cropD*(I2_sf/I1_sf)) round(I2_cropD*(I2_sf/I1_sf))];
% Img1Ann = insertShape((Img1),'Rectangle',position1,'LineWidth',12,'Color', {'red','green'});
% position2 = [round(I3_c1y/I1_sf) round(I3_c1x/I1_sf) round(I3_cropA*(I3_sf/I1_sf)) round(I3_cropA*(I3_sf/I1_sf)); round(I3_c2y/I1_sf) round(I3_c2x/I1_sf) round(I3_cropA*(I3_sf/I1_sf)) round(I3_cropA*(I3_sf/I1_sf))];
% Img1Ann = insertShape((Img1Ann),'Rectangle',position2,'Color', {'cyan','magenta'},'LineWidth',10);

% Add the scale bar to the image
Img1Ann = insertShape((Img1),'FilledRectangle',[I1y-I1_scale_xps I1x-I1_scale_yps round(I1_scale/(PixS*I1_sf)) I1_scale_h/50],'Color', {'red'},'Opacity',1);
Img1Ann = insertText(Img1Ann,[I1y-I1_scale_xps-I1_txt_xps I1x-I1_scale_yps-I1_txt_spc],[num2str(I1_scale) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');
% figure(7); subplot(2,5,[1,2,6,7]); imshow(Img1Ann); axis image; axis off;

% Update the 4k full image
I1_Trim = 0;
Img4k(:,1+50:I1y-I1_Trim,:) = Img1Ann(floor((I1x-I4kx)/2)+1:floor((I1x+I4kx)/2),1+50:I1y-I1_Trim,:); 
% figure(); imshow(Img4k); axis image; 

%% First crops with annotations

I_fish0_green_p = imcrop(I_fish0_green, [120,120,160,160]);
Img2 = imcrop(imresize(I_fish0_green_p, 1.6*2.5063), [2,2,641,641]);
% Img2 = insertShape(Img2,'Rectangle',[1 1 I2_cropD*1.6 I2_cropD*1.6],'LineWidth',15,'Color', {'red'});

% Add the annotation boxes to the image
% position1 = [round((I3_c1y-I2_c1y)/I2_sf) round((I3_c1x-I2_c1x)/I2_sf) round(I3_cropA*(I3_sf/I2_sf)) round(I3_cropA*(I3_sf/I2_sf))];
% Img2 = insertShape(Img2,'Rectangle',position1,'LineWidth',6,'Color', {'cyan'});

Img4k(41:40+round(I2_cropD*1.6),2301:2300+round(I2_cropD*1.6),:) = Img2;
% figure(); imshow(Img4k); axis image; 

%
I_fish0_red_p = imcrop(I_fish0_red, [120,120,160,160]);
Img3 = imcrop(imresize(I_fish0_red_p, 1.6*2.5063), [2,2,641,641]);
% Img3 = insertShape(Img3,'Rectangle',[1 1 I2_cropD I2_cropD],'LineWidth',15,'Color', {'green'});

% Add the annotation boxes to the image
% position1 = [round((I3_c2y-I2_c2y)/I2_sf) round((I3_c2x-I2_c2x)/I2_sf) round(I3_cropA*(I3_sf/I2_sf)) round(I3_cropA*(I3_sf/I2_sf))];
% Img3 = insertShape(Img3,'Rectangle',position1,'LineWidth',6,'Color', {'magenta'});

Img4k(41:40+round(I2_cropD*1.6),3101:3100+round(I2_cropD*1.6),:) = Img3;
% figure(); imshow(Img4k); axis image; 

% Add the scale bar to the image
% I2_scale = 10; % in mm
% I2_scale_h = 20; % in pixels
% I2_scale_xps = 350; % x position shift from bottom right corner
% I2_scale_yps = 50; % y position shift from bottom right corner
% I2_txt_spc = 100;
% I2_txt_xps = 0;
% 
% Img3 = insertShape(Img3,'FilledRectangle',[I2_cropD-I2_scale_xps I2_cropD-I2_scale_yps round(I2_scale/(PixS*I2_sf)) I2_scale_h],'Color', {'red'},'Opacity',1);
% Img3 = insertText(Img3,[I2_cropD-I2_scale_xps-I2_txt_xps I2_cropD-I2_scale_yps-I2_txt_spc],[num2str(I2_scale) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');
% 
% Img4k(I2_cropD+121:I2_cropD+120+I2_cropD,1751:1750+I2_cropD,:) = Img3; %figure(1); imshow(Img4k); axis image; 

%% second crops

I_fish1_green_p = imcrop(I_fish1_green, [120,120,160,160]);
Img4 = imcrop(imresize(I_fish1_green_p, 1.6*2.5063), [2,2,641,641]);
% Img4 = imresize(Img4,I3_cropD/I3_cropA);
% Img4 = insertShape(Img4,'Rectangle',[1 1 I3_cropD I3_cropD],'LineWidth',15,'Color', {'cyan'});

Img4k(761:760+round(I2_cropD*1.6),2301:2300+round(I2_cropD*1.6),:) = Img4; %figure(1); imshow(Img4k); axis image; 
% figure(); imshow(Img4k); axis image;

%

I_fish1_red_p = imcrop(I_fish1_red, [120,120,160,160]);
Img5 = imcrop(imresize(I_fish1_red_p, 1.6*2.5063), [2,2,641,641]);
% Img5 = imresize(Img5,I3_cropD/I3_cropA);
% Img5 = insertShape(Img5,'Rectangle',[1 1 I3_cropD I3_cropD],'LineWidth',15,'Color', {'magenta'});

Img4k(761:760+round(I2_cropD*1.6),3101:3100+round(I2_cropD*1.6),:) = Img5; 
% figure(); imshow(Img4k); axis image;

% Add the scale bar to the image
% I3_scale = 1; % in mm
% I3_scale_h = 20; % in pixels
% I3_scale_xps = 300; % x position shift from bottom right corner
% I3_scale_yps = 50; % y position shift from bottom right corner
% I3_txt_spc = 100;
% I3_txt_xps = 10;
% 
% Img5 = insertShape(Img5,'FilledRectangle',[I3_cropD-I3_scale_xps I3_cropD-I3_scale_yps round(I3_scale/(PixS/(I3_cropD/I3_cropA)*I3_sf)) I3_scale_h],'Color', {'red'},'Opacity',1);
% Img5 = insertText(Img5,[I3_cropD-I3_scale_xps-I3_txt_xps I3_cropD-I3_scale_yps-I3_txt_spc],[num2str(I3_scale) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');
% 
% Img4k(I3_cropD+121:I3_cropD+120+I3_cropD,1751+I2_cropD+50:1750+I2_cropD+50+I3_cropD,:) = Img5;
% figure(1); imshow(Img4k); axis image;

%% third crops
I_fish2_green_p = imcrop(I_fish2_green, [120,120,160,160]);
Img6 = imcrop(imresize(I_fish2_green_p, 1.6*2.5063), [2,2,641,641]);

Img4k(1481:1480+round(I2_cropD*1.6),2301:2300+round(I2_cropD*1.6),:) = Img6;
% figure(); imshow(Img4k); axis image;

% Add the scale bar to the image
I7_scale = 1; % in mm
I7_scale_h = 20; % in pixels
I7_scale_xps = 250; % x position shift from bottom right corner
I7_scale_yps = 50; % y position shift from bottom right corner
I7_txt_spc = 100;
I7_txt_xps = 0;

I_fish2_red_p = imcrop(I_fish2_red, [120,120,160,160]);
Img7 = imcrop(imresize(I_fish2_red_p, 1.6*2.5063), [2,2,641,641]);

Img7 = insertShape(Img7,'FilledRectangle',[round(I2_cropD*1.6)-I7_scale_xps round(I2_cropD*1.6)-I7_scale_yps round(I1_scale/(PixS*I1_sf)) I1_scale_h/50],'Color', {'red'},'Opacity',1);
Img7 = insertText(Img7,[I2_cropD*1.6-I7_scale_xps-I7_txt_xps-20 I2_cropD*1.6-I7_scale_yps-I7_txt_spc],[num2str(I7_scale/2.5) ' mm'],'BoxOpacity',0,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','red');

Img4k(1481:1480+round(I2_cropD*1.6),3101:3100+round(I2_cropD*1.6),:) = Img7;
% figure(); imshow(Img4k); axis image;


%%
% Img4k = insertText(Img4k,[760 10],['Frame number: ' n_pad],'BoxColor','white','BoxOpacity',1,'Font','LucidaSansDemiBold', 'FontSize',60,'TextColor','black');  figure(1); imshow(Img4k); axis image; 

FileN2 = [FolB '01_Stitched_Data\' 'Video_try2\movie0_stitched_' num2str(n_pad) '_videoSnap.png'];
imwrite(Img4k,FileN2);

Img4k = insertText(Img4k,[730 10],['Frame number: ' num2str(n_pad)],'BoxColor','white','BoxOpacity',0.8,'Font','LucidaSansDemiBold', 'FontSize',50,'TextColor','black');  
Img4k = insertText(Img4k,[2100 310],'Fish 0','BoxColor','white','Font','LucidaSansDemiBold', 'FontSize',50,'TextColor','black');  
Img4k = insertText(Img4k,[2100 1040],'Fish 1','BoxColor','white','Font','LucidaSansDemiBold', 'FontSize',50,'TextColor','black');  
Img4k = insertText(Img4k,[2100 1760],'Fish 2','BoxColor','white','Font','LucidaSansDemiBold', 'FontSize',50,'TextColor','black');  

% figure(); imshow(Img4k); axis image; 
writeVideo(v, Img4k);
%%
end
close(v);

% figure(1); imshow(Img4k); axis image;


toc