# -*- coding: utf-8 -*-
"""
Created on Thu Dec 30 18:50:11 2021

@author: kanghyun
"""

import os
import glob
import cv2
import numpy as np
from matplotlib import cm
from scipy import ndimage
from sklearn.decomposition import PCA
from pystackreg import StackReg
from scipy import signal
import pandas as pd
from matplotlib import pyplot as plt


#%%
def normalize_image(image, max_value):
    image = image.astype(float)
    image[image > max_value] = max_value
    image /= max_value
    return (image*255).astype(np.uint8)

def get_cropped_ROI_with_padding(thresh_array, square_size, x, y, w, h):
    if w > 2 * square_size + 1:
        w = 2 * square_size + 1
    if h > 2 * square_size + 1:
        h = 2 * square_size + 1
    cropped_ROI = np.zeros((2 * square_size + 1, 2 * square_size + 1), dtype = np.uint8)
    cropped_ROI[square_size - h//2:square_size + np.ceil(h/2).astype(int), square_size - w//2:square_size + np.ceil(w/2).astype(int)]\
        = thresh_array[y:y+h, x:x+w]
    return cropped_ROI

def padarray(array, size):
    row_t = size - array.shape[0]
    col_t = size - array.shape[1]
    if row_t != 0:
        array = np.pad(array, ((row_t//2, np.ceil(row_t/2).astype(int)), (0, 0), (0, 0)), mode='constant')
    if col_t != 0:
        array = np.pad(array, ((0, 0), (col_t//2, np.ceil(col_t/2).astype(int)), (0, 0)), mode='constant')
    return array

def get_rotation_angle_from_PCA_eigenvector(image):
    y, x = np.where(image != 0)
    pca = PCA(n_components=2)
    pca.fit(np.stack([x, -y], axis = 1))
    rot_angle = np.degrees(np.arccos(pca.components_[1,1]))
    return rot_angle

def align_fish_with_PCA(image):
    rot_angle_first = get_rotation_angle_from_PCA_eigenvector(image)
    rotated_image_first = ndimage.rotate(image, rot_angle_first, reshape=False)
    
    rot_angle_second = get_rotation_angle_from_PCA_eigenvector(rotated_image_first)
    if rot_angle_second > 1 and rot_angle_second < 179:
        return ndimage.rotate(image, -rot_angle_first, reshape=False), -rot_angle_first
    else:
        return rotated_image_first, rot_angle_first


#%% load all stitched images to process
parent_dir = '.\\'
data_dir = ''

load_dir = os.path.join(parent_dir, data_dir, 'green')
stitched_img_path_green = glob.glob(os.path.join(load_dir, '*Panorama.jpg'))
stitched_img_path_green.sort()
load_dir = os.path.join(parent_dir, data_dir, 'red')
stitched_img_path_red = glob.glob(os.path.join(load_dir, '*Panorama.jpg'))
stitched_img_path_red.sort()


#%% generate fish contour and save segmented fish image
green_background_threshold = 9
red_background_threshold = 9

patch_size = 1200
green_min, green_max = 40, 500
red_min, red_max = 40, 500
col_margin_left = 150
col_margin_right = 4600
max_green = 60
max_red = 60
area_diag_ratio = 15
len_diag_ratio = 5

number_of_fishes = 3

# make directories for saving results
save_dir = os.path.join(parent_dir, data_dir, 'green_cnts')
if not os.path.exists(save_dir):
    os.mkdir(save_dir)
save_dir = os.path.join(parent_dir, data_dir, 'red_cnts')
if not os.path.exists(save_dir):
    os.mkdir(save_dir)
save_dir = os.path.join(parent_dir, data_dir, 'all_fishes_segmentation_gr')
if not os.path.exists(save_dir):
    os.mkdir(save_dir)

for i, paths in enumerate(zip(stitched_img_path_green, stitched_img_path_red)):
    path_green = paths[0]
    path_red = paths[1]
    image_raw_green = cv2.imread(path_green)
    image_raw_red = cv2.imread(path_red)
    
    # remove background
    image_raw_green = image_raw_green[3533:14333, 1000:5800]
    image_raw_red = image_raw_red[2715:,3500:8300]
     
    # get threshold
    image_raw_green_gray = np.copy(image_raw_green[:,:,1])
    image_raw_red_gray = np.copy(image_raw_red[:,:,2])
    thresh_green_gray = np.zeros(image_raw_green_gray.shape, dtype = np.uint8)
    thresh_red_gray = np.zeros(image_raw_red_gray.shape, dtype = np.uint8)
    patch_row_indexes = np.linspace(0, 10800, num=10, dtype = int)[:-1]
    patch_col_indexes = np.linspace(0, 4800, num=5, dtype = int)[:-1]
    for patch_row_index in patch_row_indexes:
        for patch_col_index in patch_col_indexes:
            image_raw_green_patch = image_raw_green_gray[patch_row_index:patch_row_index+patch_size, patch_col_index:patch_col_index+patch_size]
            image_raw_red_patch = image_raw_red_gray[patch_row_index:patch_row_index+patch_size, patch_col_index:patch_col_index+patch_size]
            
            thresh_green_gray[patch_row_index:patch_row_index+patch_size, patch_col_index:patch_col_index+patch_size] = \
                cv2.threshold(image_raw_green_patch,green_background_threshold,255,cv2.THRESH_BINARY)[1]
            thresh_red_gray[patch_row_index:patch_row_index+patch_size, patch_col_index:patch_col_index+patch_size] = \
                cv2.threshold(image_raw_red_patch,red_background_threshold,255,cv2.THRESH_BINARY)[1]
    
    # find contours
    cnts_green_gray = cv2.findContours(thresh_green_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts_green_gray = cnts_green_gray[0] if len(cnts_green_gray) == 2 else cnts_green_gray[1]
    cnts_red_gray = cv2.findContours(thresh_red_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts_red_gray = cnts_red_gray[0] if len(cnts_red_gray) == 2 else cnts_red_gray[1]
    
    # get only fish contours
    cnts_green_gray_fish = []
    cnts_red_gray_fish = []
    centers_green = []
    centers_red = []
    stds_green = []
    stds_red = []
    for c in cnts_green_gray:
        x,y,w,h = cv2.boundingRect(c)
        length = cv2.arcLength(c,True)
        area = np.sum(thresh_green_gray[y:y+h, x:x+w] / 255)
        if (w < green_min or h < green_min) or (w > green_max or h > green_max) or (length / (np.sqrt(w ** 2 + h ** 2)) > len_diag_ratio)\
        or (x < col_margin_left or x > col_margin_right) or (area / np.sqrt(w ** 2 + h ** 2) < area_diag_ratio): #  or (length / (w * h) > 0.12) # 
            continue
        cnts_green_gray_fish.append(c)
        centers_green.append([x + w//2, y + h//2])
        stds_green.append(np.std(image_raw_green_gray[y:y+h, x:x+w]))
    for c in cnts_red_gray:
        x,y,w,h = cv2.boundingRect(c)
        length = cv2.arcLength(c,True)
        area = np.sum(thresh_red_gray[y:y+h, x:x+w] / 255)
        if (w < red_min or h < red_min) or (w > red_max or h > red_max) or (length / (np.sqrt(w ** 2 + h ** 2)) > len_diag_ratio)\
        or (x < col_margin_left or x > col_margin_right) or (area / np.sqrt(w ** 2 + h ** 2) < area_diag_ratio): # added area_diag_ratio
            continue
        cnts_red_gray_fish.append(c)
        centers_red.append([x + w//2, y + h//2])
        stds_red.append(np.std(image_raw_red_gray[y:y+h, x:x+w]))
    cnts_green_gray_fish = np.array(cnts_green_gray_fish)
    cnts_red_gray_fish = np.array(cnts_red_gray_fish)
    centers_green = np.array(centers_green)
    centers_red = np.array(centers_red)
    stds_green = np.array(stds_green)
    stds_red = np.array(stds_red)
    
    # sort contours - get the 3 fishes from red only
    ind_red = np.argsort(-stds_red)[:number_of_fishes]
    cnts_red_gray_fish = cnts_red_gray_fish[ind_red]
    centers_red = centers_red[ind_red]
    # sort contours - sort fishes from top left
    ind_red = np.lexsort((-centers_red[:,0],-centers_red[:,1]))
    cnts_red_gray_fish = cnts_red_gray_fish[ind_red]
    centers_red = centers_red[ind_red]
        
    # find closest green fish from each red
    ind_green = []
    losses_found_fish = np.zeros(len(cnts_green_gray_fish))
    for j, (c_red, center_red) in enumerate(zip(cnts_red_gray_fish, centers_red)):
        losses = []
        x_red,y_red,w_red,h_red = cv2.boundingRect(c_red)
        ROI_red = image_raw_red_gray[y_red:y_red+h_red, x_red:x_red+w_red]
        ROI_thresh_red = thresh_red_gray[y_red:y_red+h_red, x_red:x_red+w_red]
        ROI_red = (ROI_red * ROI_thresh_red).astype(float)
        ROI_red[ROI_red == 0] = np.nan
        for c_green in cnts_green_gray_fish:
            x_green,y_green,w_green,h_green = cv2.boundingRect(c_green)
            ROI_green = image_raw_green_gray[y_green:y_green+h_green, x_green:x_green+w_green]
            ROI_thresh_green = thresh_green_gray[y_green:y_green+h_green, x_green:x_green+w_green]
            ROI_green = (ROI_green * ROI_thresh_green).astype(float)
            ROI_green[ROI_green == 0] = np.nan
        where_green = np.argmin(losses_found_fish + np.sum(np.power(centers_green - center_red, 2), axis = 1))
        losses_found_fish[where_green] = np.inf
        ind_green.append(where_green)
    cnts_green_gray_fish = cnts_green_gray_fish[ind_green]
    centers_green = centers_green[ind_green]
    
    # save contours
    green_path = os.path.join(parent_dir, data_dir, 'green_cnts', 'cnts_' + str(i) + '.npy')
    red_path = os.path.join(parent_dir, data_dir, 'red_cnts', 'cnts_' + str(i) + '.npy')
    np.save(green_path, cnts_green_gray_fish)
    np.save(red_path, cnts_red_gray_fish)
    
    # normalize image before plotting
    image_raw_green = normalize_image(image_raw_green, max_green)
    image_raw_red = normalize_image(image_raw_red, max_red)
    image_raw_green_gray = normalize_image(image_raw_green_gray, max_green)
    image_raw_red_gray = normalize_image(image_raw_red_gray, max_red)
    
    # divide green and red and get ratio
    image_raw_green_gray[image_raw_green_gray == 0] = 1
    image_raw_red_gray[image_raw_red_gray == 0] = 1
    image_ratio = np.log(image_raw_green_gray / image_raw_red_gray)
    
    # colormap rgba to bgra
    c = cm.gist_rainbow(np.linspace(0, 1, len(cnts_red_gray_fish)))*255
    c = np.stack([c[:,2], c[:,1], c[:,0], c[:,3]], axis=1)   
    
    # obtain bounding box, extract ROI
    for k, (c_green, c_red) in enumerate(zip(cnts_green_gray_fish, cnts_red_gray_fish)): # cnts_green_gray_fish
        x_green,y_green,w_green,h_green = cv2.boundingRect(c_green)
        ROI_green = image_raw_green[y_green:y_green+h_green, x_green:x_green+w_green]
        cv2.rectangle(image_raw_green, (x_green, y_green), (x_green + w_green, y_green + h_green), c[k], 3)
        cv2.putText(image_raw_green, str(k), (x_green + w_green//2, y_green + h_green), cv2.FONT_HERSHEY_SIMPLEX, 15, c[k], 15)
        cv2.drawContours(image_raw_green, c_green, -1, c[k], 3) # thickness=cv2.FILLED
        x_red,y_red,w_red,h_red = cv2.boundingRect(c_red)
        ROI_red = image_raw_red[y_red:y_red+h_red, x_red:x_red+w_red]
        cv2.rectangle(image_raw_red, (x_red, y_red), (x_red + w_red, y_red + h_red), c[k], 3)
        cv2.putText(image_raw_red, str(k), (x_red + w_red//2, y_red + h_red), cv2.FONT_HERSHEY_SIMPLEX, 15, c[k], 15)
        cv2.rectangle(image_ratio, (x_red, y_red), (x_red, y_red), c[k], 3)
        cv2.drawContours(image_raw_red, c_red, -1, c[k], 3) 
    
    cv2.imwrite(os.path.join(parent_dir, data_dir, f'all_fishes_segmentation_gr\\frame_{i}.jpg'), np.concatenate((image_raw_green, image_raw_red), axis=1))


#%% save the reference images
start_track_fish_indices = [0,1,2]
frame_as_reference = [242,87,6]
rotation_for_reference = [255,236,15]

save_dir = os.path.join(parent_dir, data_dir)

for i, (start_track_fish_index, frame, rotation) in enumerate(zip(start_track_fish_indices, frame_as_reference, rotation_for_reference)):
    fish_info = pd.read_csv(os.path.join(parent_dir, data_dir, 'fish_info_sheets', f'fish{start_track_fish_index}_track_original.csv'))
    fish_info = fish_info.to_numpy()

    path_green = stitched_img_path_green[frame]
    path_red = stitched_img_path_red[frame]
    image_raw_green = cv2.imread(path_green)#.astype(float)
    image_raw_red = cv2.imread(path_red)#.astype(float)
    
    # remove background
    image_raw_green = image_raw_green[3533:14333, 1000:5800]
    image_raw_red = image_raw_red[2715:,3500:8300]
     
    # get each channel
    image_raw_green_gray = np.copy(image_raw_green[:,:,1])
    image_raw_red_gray = np.copy(image_raw_red[:,:,2])
    
    # load contours
    green_path = os.path.join(parent_dir, data_dir, 'green_cnts', 'cnts_' + str(frame) + '.npy')
    red_path = os.path.join(parent_dir, data_dir, 'red_cnts', 'cnts_' + str(frame) + '.npy')
    cnts_green_gray_fish = np.load(green_path, allow_pickle=True)
    cnts_red_gray_fish = np.load(red_path, allow_pickle=True)
    
    # get countour info
    x_green,y_green,w_green,h_green = cv2.boundingRect(cnts_green_gray_fish[int(fish_info[frame,2])])
    x_red,y_red,w_red,h_red = cv2.boundingRect(cnts_red_gray_fish[int(fish_info[frame,3])])
    
    # fill the contours on the empty board
    thresh_green_gray = np.zeros(image_raw_green_gray.shape, dtype = np.uint8)
    thresh_red_gray = np.zeros(image_raw_red_gray.shape, dtype = np.uint8)
    thresh_green_gray = cv2.drawContours(thresh_green_gray, [cnts_green_gray_fish[int(fish_info[frame,2])]], -1, color=(255,255,255),thickness=-1)
    thresh_red_gray = cv2.drawContours(thresh_red_gray, [cnts_red_gray_fish[int(fish_info[frame,3])]], -1, color=(255,255,255),thickness=-1)
    
    refernce_image_green = get_cropped_ROI_with_padding(normalize_image(image_raw_green_gray, max_green) * thresh_green_gray.astype(bool), 200, x_green, y_green, w_green, h_green)
    refernce_image_red = get_cropped_ROI_with_padding(normalize_image(image_raw_red_gray, max_red) * thresh_red_gray.astype(bool), 200, x_red, y_red, w_red, h_red)
    refernce_image_green = ndimage.rotate(refernce_image_green, rotation, reshape=False)
    refernce_image_red = ndimage.rotate(refernce_image_red, rotation, reshape=False)
    
    cv2.imwrite(os.path.join(save_dir, 'green', f'fish{start_track_fish_index}_reference.jpg'), refernce_image_green)
    cv2.imwrite(os.path.join(save_dir, 'red', f'fish{start_track_fish_index}_reference.jpg'), refernce_image_red)


#%% get energy of green and red channel / plot aligned images
start_track_fish_index = 0 # pick either 0, 1, or 2

center_green = []
center_red = []

fish_info = pd.read_csv(os.path.join(parent_dir, data_dir, 'fish_info_sheets', f'fish{start_track_fish_index}_track_original.csv'))
fish_info = fish_info.to_numpy()
fish_info = np.concatenate((fish_info, np.zeros((fish_info.shape[0],4))), axis = 1)

save_dir = os.path.join(parent_dir, data_dir, 'all_fishes_registration_gr')
if not os.path.exists(save_dir):
    os.mkdir(save_dir)
if not os.path.exists(save_dir + f'\\fish{start_track_fish_index}_registration_gr'):
    os.mkdir(save_dir + f'\\fish{start_track_fish_index}_registration_gr')
save_dir = os.path.join(parent_dir, data_dir, 'all_fishes_rotated')
if not os.path.exists(save_dir):
    os.mkdir(save_dir)
if not os.path.exists(save_dir + f'\\fish{start_track_fish_index}_rotated'):
    os.mkdir(save_dir + f'\\fish{start_track_fish_index}_rotated')
if not os.path.exists(os.path.join(save_dir + f'\\fish{start_track_fish_index}_rotated', 'green')):
    os.mkdir(os.path.join(save_dir + f'\\fish{start_track_fish_index}_rotated', 'green'))
if not os.path.exists(os.path.join(save_dir + f'\\fish{start_track_fish_index}_rotated', 'red')):
    os.mkdir(os.path.join(save_dir + f'\\fish{start_track_fish_index}_rotated', 'red'))

start = True
for i, paths in enumerate(zip(stitched_img_path_green, stitched_img_path_red)):
    if fish_info[i,1] == 1:
        continue
    
    path_green = paths[0]
    path_red = paths[1]
    image_raw_green = cv2.imread(path_green)
    image_raw_red = cv2.imread(path_red)
    
    # remove background
    image_raw_green = image_raw_green[3533:14333, 1000:5800]
    image_raw_red = image_raw_red[2715:,3500:8300]
     
    # get each channel
    image_raw_green_gray = np.copy(image_raw_green[:,:,1])
    image_raw_red_gray = np.copy(image_raw_red[:,:,2])
    
    # load contours
    green_path = os.path.join(parent_dir, data_dir, 'green_cnts', 'cnts_' + str(i) + '.npy')
    red_path = os.path.join(parent_dir, data_dir, 'red_cnts', 'cnts_' + str(i) + '.npy')
    cnts_green_gray_fish = np.load(green_path, allow_pickle=True)
    cnts_red_gray_fish = np.load(red_path, allow_pickle=True)
    
    # get center position
    x_green,y_green,w_green,h_green = cv2.boundingRect(cnts_green_gray_fish[int(fish_info[i,2])])
    x_red,y_red,w_red,h_red = cv2.boundingRect(cnts_red_gray_fish[int(fish_info[i,3])])
    center_green.append([x_green + w_green//2, y_green + h_green//2])
    center_red.append([x_red + w_red//2, y_red + h_red//2])
    
    # fill the contours on the empty board
    thresh_green_gray = np.zeros(image_raw_green_gray.shape, dtype = np.uint8)
    thresh_red_gray = np.zeros(image_raw_red_gray.shape, dtype = np.uint8)
    thresh_green_gray = cv2.drawContours(thresh_green_gray, [cnts_green_gray_fish[int(fish_info[i,2])]], -1, color=(255,255,255),thickness=-1)
    thresh_red_gray = cv2.drawContours(thresh_red_gray, [cnts_red_gray_fish[int(fish_info[i,3])]], -1, color=(255,255,255),thickness=-1)
    
    # load the reference (aligned with translation + rotation) images
    if start:
        refernce_image_green = cv2.imread(os.path.join(parent_dir, data_dir, 'green', f'fish{start_track_fish_index}_reference.jpg'), 0)
        refernce_image_red = cv2.imread(os.path.join(parent_dir, data_dir, 'red', f'fish{start_track_fish_index}_reference.jpg'), 0)
    
    input_image_green = get_cropped_ROI_with_padding(normalize_image(image_raw_green_gray, max_green) * thresh_green_gray.astype(bool), 200, x_green, y_green, w_green, h_green)
    input_image_red = get_cropped_ROI_with_padding(normalize_image(image_raw_red_gray, max_red) * thresh_red_gray.astype(bool), 200, x_red, y_red, w_red, h_red)
    
    input_image_green, _ = align_fish_with_PCA(input_image_green)
    input_image_red, _ = align_fish_with_PCA(input_image_red)
    input_image_green = ndimage.rotate(input_image_green, 90, reshape=False)
    input_image_red = ndimage.rotate(input_image_red, 90, reshape=False)
    if np.sum(input_image_green[:201,:]) < np.sum(input_image_green[201:,:]):
        input_image_green = np.rot90(input_image_green, 2)
    if np.sum(input_image_red[:201,:]) < np.sum(input_image_red[201:,:]):
        input_image_red = np.rot90(input_image_red, 2)
    
    sr = StackReg(StackReg.TRANSLATION)
    transformed_image_green = sr.register_transform(refernce_image_green, input_image_green)
    transformed_image_red = sr.register_transform(refernce_image_red, input_image_red)
    transformed_image_green = np.stack([np.zeros((401, 401)), transformed_image_green, np.zeros((401, 401))], axis = 2)
    transformed_image_red = np.stack([np.zeros((401, 401)), np.zeros((401, 401)), transformed_image_red], axis = 2)
    cv2.imwrite(os.path.join(save_dir + f'\\fish{start_track_fish_index}_rotated', 'green', f'frame_{i}.jpg'), transformed_image_green)
    cv2.imwrite(os.path.join(save_dir + f'\\fish{start_track_fish_index}_rotated', 'red', f'frame_{i}.jpg'), transformed_image_red)
    
    start = False
    
    # crop near the ROI
    square_size = np.max((w_green, h_green, w_red, h_red))
    cropped_green_ROI = get_cropped_ROI_with_padding(thresh_green_gray, square_size, x_green, y_green, w_green, h_green)
    cropped_red_ROI = get_cropped_ROI_with_padding(thresh_red_gray, square_size, x_red, y_red, w_red, h_red)
    convolution_result = signal.correlate(cropped_green_ROI.astype(float), cropped_red_ROI.astype(float), mode='same')
    conv_max_point = np.where(convolution_result == np.max(convolution_result))
    conv_max_point = [np.mean(conv_max_point[0], dtype = int), np.mean(conv_max_point[1], dtype = int)]
    cropped_green_ROI = np.roll(cropped_green_ROI, -(conv_max_point[0] - square_size), axis=0)
    cropped_green_ROI = np.roll(cropped_green_ROI, -(conv_max_point[1] - square_size), axis=1)
    inclusive_ROI = np.logical_or(cropped_green_ROI, cropped_red_ROI)
    cropped_green_ROI = np.roll(inclusive_ROI, (conv_max_point[0] - square_size), axis=0)
    cropped_green_ROI = np.roll(cropped_green_ROI, (conv_max_point[1] - square_size), axis=1)
    cropped_red_ROI = inclusive_ROI
    
    thresh_green_gray[max(center_green[-1][1]-square_size, 0):min(center_green[-1][1]+square_size+1, image_raw_green.shape[0]),\
                      max(center_green[-1][0]-square_size, 0):min(center_green[-1][0]+square_size+1, image_raw_green.shape[1])]\
        = cropped_green_ROI[np.abs(center_green[-1][1]-square_size) if (center_green[-1][1]-square_size < 0) else 0:\
                            (2*square_size+1 - (center_green[-1][1]+square_size+1 - image_raw_green.shape[0])) if (center_green[-1][1]+square_size+1 > image_raw_green.shape[0]) else 2*square_size+1,\
                            np.abs(center_green[-1][0]-square_size) if (center_green[-1][0]-square_size < 0) else 0:\
                            (2*square_size+1 - (center_green[-1][0]+square_size+1 - image_raw_green.shape[1])) if (center_green[-1][0]+square_size+1 > image_raw_green.shape[1]) else 2*square_size+1]
    thresh_red_gray[max(center_red[-1][1]-square_size, 0):min(center_red[-1][1]+square_size+1, image_raw_red.shape[0]),\
                      max(center_red[-1][0]-square_size, 0):min(center_red[-1][0]+square_size+1, image_raw_red.shape[1])]\
        = cropped_red_ROI[np.abs(center_red[-1][1]-square_size) if (center_red[-1][1]-square_size < 0) else 0:\
                            (2*square_size+1 - (center_red[-1][1]+square_size+1 - image_raw_red.shape[0])) if (center_red[-1][1]+square_size+1 > image_raw_red.shape[0]) else 2*square_size+1,\
                            np.abs(center_red[-1][0]-square_size) if (center_red[-1][0]-square_size < 0) else 0:\
                            (2*square_size+1 - (center_red[-1][0]+square_size+1 - image_raw_red.shape[1])) if (center_red[-1][0]+square_size+1 > image_raw_red.shape[1]) else 2*square_size+1]
    
    # save energy
    green_ROI = image_raw_green_gray * thresh_green_gray.astype(bool)
    red_ROI = image_raw_red_gray * thresh_red_gray.astype(bool)
    fish_info[i,4] = np.sum(green_ROI)
    fish_info[i,5] = np.sum(red_ROI)
    fish_info[i,6] = np.sum(green_ROI) / np.sum(red_ROI)
    
    # normalize image before plotting
    image_raw_green = normalize_image(image_raw_green, max_green)
    image_raw_red = normalize_image(image_raw_red, max_red)
    
    # colormap rgba to bgra
    c = cm.gist_rainbow(np.linspace(0, 1, len(cnts_red_gray_fish)))*255
    c = np.stack([c[:,2], c[:,1], c[:,0], c[:,3]], axis=1)   
    
    # extract ROI and draw contour
    k = 0
    center_diff = np.array([x_green + w_green//2, y_green + h_green//2]) - np.array([x_red + w_red//2, y_red + h_red//2])
    c_green = cnts_green_gray_fish[int(fish_info[i,2])]
    c_red = cnts_red_gray_fish[int(fish_info[i,3])]
    x_green,y_green,w_green,h_green = cv2.boundingRect(c_green)
    image_raw_green = image_raw_green * thresh_green_gray[:,:,None]
    ROI_green = image_raw_green[center_green[-1][1]-square_size:center_green[-1][1]+square_size+1,\
                                center_green[-1][0]-square_size:center_green[-1][0]+square_size+1]
    cv2.drawContours(image_raw_green, c_green, -1, c[k+1], 3)
    c_red_temp = np.copy(c_red)
    c_red_temp = c_red_temp + center_diff + (np.array(conv_max_point)[None,::-1] - np.array([square_size, square_size])[None,::-1])
    cv2.drawContours(image_raw_green, c_red_temp, -1, c[k+0], 3)
    x_red,y_red,w_red,h_red = cv2.boundingRect(c_red)
    image_raw_red = image_raw_red * thresh_red_gray[:,:,None]
    ROI_red = image_raw_red[center_red[-1][1]-square_size:center_red[-1][1]+square_size+1,\
                            center_red[-1][0]-square_size:center_red[-1][0]+square_size+1]
    c_green_temp = np.copy(c_green)
    c_green_temp = c_green_temp - center_diff - (np.array(conv_max_point)[None,::-1] - np.array([square_size, square_size])[None,::-1])
    cv2.drawContours(image_raw_red, c_green_temp, -1, c[k+1], 3)
    cv2.drawContours(image_raw_red, c_red, -1, c[k+0], 3)
    
    cv2.imwrite(os.path.join(parent_dir, data_dir, f'all_fishes_registration_gr\\fish{start_track_fish_index}_registration_gr\\frame_{i}.jpg'),\
                np.concatenate((padarray(ROI_green, 2*square_size+1), padarray(ROI_red, 2*square_size+1)), axis=1))

center_green = np.array(center_green)
center_red = np.array(center_red)
speed_green = np.sqrt(np.sum((center_green[1:] - center_green[:-1])**2, axis=1))

where = 0
num_of_missing = 0
for i in range(len(stitched_img_path_green)):
    if fish_info[i,1] == 1 or i == 0:
        if fish_info[i,1] == 1:
            num_of_missing += 1
        continue
    
    fish_info[i,7] = speed_green[where]/(num_of_missing+1) if num_of_missing else speed_green[where]
    where += 1
    num_of_missing = 0

fish_info = pd.DataFrame(data=fish_info, columns=['frame', 'pass_frame', 'green_index', 'red_index', 'green_energy', 'red_energy', 'ratio', 'speed'])
fish_info.to_csv(os.path.join(parent_dir, data_dir, 'fish_info_sheets', f'fish{start_track_fish_index}_track.csv'), index=False)


#%% plot line plot - actual speed, no smoothing
start_track_fish_index = 0 # pick either 0, 1, or 2
clip_max = 100

fish_info = pd.read_csv(os.path.join(parent_dir, data_dir, 'fish_info_sheets', f'fish{start_track_fish_index}_track.csv'))
fish_info_processing = np.copy(fish_info.to_numpy())

fish_info_processing = fish_info_processing[fish_info_processing[:,1] == 0]

fish_info_processing[:,6] = fish_info_processing[:,4] / fish_info_processing[:,5]
fish_info_processing = fish_info_processing[5:,]
fish_info_processing = fish_info_processing[:-5,]

t = np.arange(0, fish_info_processing.shape[0], 1)
fig = plt.figure()
ax1 = fig.add_subplot(111)

color = 'tab:green'
#ax1.set_xlabel('time (s)')
ax1.set_ylabel('ratio', color=color)
ax1.plot(t, fish_info_processing[:,6], '--', color=color, alpha=0.8)
ax1.get_xaxis().set_visible(False)
ax1.tick_params(axis='y', labelcolor=color)
ax1.set_ylim([0.65,1])

ax2 = ax1.twinx() # instantiate a second axes that shares the same x-axis

color = 'k'
ax2.set_ylabel('speed (cm/s)', color=color)  # we already handled the x-label with ax1
clipped_value = np.clip(fish_info_processing[:,7], 0, clip_max)/1000
ax2.plot(t, clipped_value, color=color, alpha=0.8)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout() # otherwise the right y-label is slightly clipped
plt.savefig('dualchannel_fish.eps', format='eps')
plt.show()


#%% tracking fish in the arena
empty_arena = np.zeros((10800, 4800))
start_track_fish_indices = [0, 1, 2]

hsv = cm.get_cmap('hsv', 256)

plt.figure(figsize=(10, 10))
plt.imshow(empty_arena, cmap="gray")
plt.axis('off')
for i, start_track_fish_index in enumerate(start_track_fish_indices):
    fish_info = pd.read_csv(os.path.join(parent_dir, data_dir, 'fish_info_sheets', f'fish{start_track_fish_index}_track_original.csv'))
    fish_info = fish_info.to_numpy()

    center_green = []
    
    for j in range(len(stitched_img_path_green)):
        if fish_info[j,1] == 1:
            continue
        
        # load contours
        green_path = os.path.join(parent_dir, data_dir, 'green_cnts', 'cnts_' + str(j) + '.npy')
        cnts_green_gray_fish = np.load(green_path, allow_pickle=True)
        
        # get center position
        x_green,y_green,w_green,h_green = cv2.boundingRect(cnts_green_gray_fish[int(fish_info[j,2])])
        center_green.append([x_green + w_green//2, y_green + h_green//2])
    
    center_green = np.array(center_green)
    
    plt.text(center_green[0,0], center_green[0,1], 'S', fontsize=15, c=hsv((i+1)*50), ha='right')
    plt.text(center_green[-1,0], center_green[-1,1], 'F', fontsize=15, c=hsv((i+1)*50), ha='right')
    plt.plot(center_green[:,0], center_green[:,1], c=hsv((i+1)*50), marker='o', markersize=3, linewidth=0.7, mfc='none', label=f'fish{start_track_fish_index}')
plt.legend(loc="upper right")
plt.savefig('./all_fishes_track.png', bbox_inches='tight', pad_inches=0)
plt.show()