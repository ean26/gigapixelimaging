MCAM images of zebrafish used for training and testing in our siamese network. Folders are
titled by fish number (e.g. f2 is fish 2). Images titled: fish number_frame number_id.

Generally, training images are cropped from the first 80% of MCAM frames during a 
recording, while testing images are cropped from the last 20% of MCAM frames. Fish are
centered in middle of images according to coordinates obtained from bounding boxes of 
Faster-RCNN object detection network. Dimensions of cropped images are 459x459 pixels - 
the maximum bounding box dimension occuring when fish are oriented horizontally or 
vertically. In the network, these images are downsampled to 224 x 224 pixels.

Data augmentation consists of random rotations with θ ≤ 360°. 250 augmented images are
generated for the 9 fish for 200 training images and 50 testing images per fish,
resulting in a total of 1800 training images and 450 testing images. The training and
testing augmented images derive from cropped images of frames separated in time.

Note, we did not train the network on images of all fish from the original MCAM
recording and instead trained the network on 9 fish for best visualisation. Fish positioned 
near an arena edge for most of the recording or had severe stitching artifacts that distorted
the fish's body were excluded. Because the Faster-RCNN object detection network (gigadetctor) assigns id based on object position, this network does not identify individual fish. Therefor, we generated ground truth data by manually assigning fish identity for each frame in the recording. Therefore some image names mismatch folder titles (e.g. folder f6 has images for fish f8). Additionally, we used fewer MCAM frames but more 
augmented images per frame to obtain 200 training images and 50 testing images for two
fish (f3 and f4) due to these fish switching positions during the recording. 



